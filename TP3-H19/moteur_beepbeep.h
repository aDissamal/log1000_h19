#include "moteur.h"

class Beepbeep : public Moteur {
    public:
        Beepbeep(int numero_serie, int nb_pistons);
        int getPuissance();
};
